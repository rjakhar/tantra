# Following PHP Component has been used to create this project:-

symfony/http-foundation

symfony/routing

symfony/http-kernel

symfony/templating

symfony/form

symfony/validator

zendframework/zend-db



### What is this repository for? ###

* Admin Login/Logout
* Simple Todo (Add/update/Delete items)
* Access :- Admin & User with Read Only permission.

#### Database Configuration
* Import the installation.sql in separate database.
* Open the file web/front.php
* Change the following details:-

define(DB_NAME,'YOUR_DB_NAME');

define(DB_USER,'YOUR_DB_USER');

define(DB_PASSWORD,'YOUR_DB_PASSWORD');

### How do I get set up? ###
You need to create a virtual host in the file /etc/apache2/sites-available/000-default.conf

Steps:-

* Open the file.

* Put the following Lines after the default virtual host

# Be sure to only have this line once in your configuration
NameVirtualHost 127.0.0.1:8181

# This is the configuration for your project
Listen 127.0.0.1:8181

<VirtualHost 127.0.0.1:8181>

  DocumentRoot /var/www/html/DIRECTORY_NAME/web

</VirtualHost>

* Restart the apache after making the configuration.
* Access the project using http://127.0.0.1:8181/front.php/admin URL.

#####  User Details ###########

1.Admin

admin@gmail.com

123456

2.User with read only permission

support@gmail.com

123456