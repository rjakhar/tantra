<?php
namespace Todo\Model;

use Zend\Db\Adapter\Adapter;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Db\ResultSet\HydratingResultSet;

class Todo
{

	/*
	  ** 
	*/
	protected $todoTable = 'todo';

	/*
      ** Define Adapter Instance 
    */
    protected $adapter;

    /*
   	  **
	*/
	protected $tblInstance;


    public function __construct()
    {	
    	$this->adapter = new Adapter(
    					array
    						(
    						'driver'   => DRIVER,
    						'database' => DB_NAME,
    						'username' => DB_USER,
    						'password' => DB_PASSWORD
						)
    				);

		$this->tblInstance = new TableGateway(
								$this->todoTable, 
								$this->adapter, 
								null,
								new HydratingResultSet()
							);
    }


    public function fetchAll(){

    	$rowset = $this->tblInstance->select();

        return $rowset->toArray();
    }

    public function add($params = null){
        
        $rowset = $this->tblInstance->insert($params);
        return true;
    }

    public function fetchOne($id = null){

        $rowset = $this->tblInstance->select(array( 'id' => $id ));
        return $rowset->toArray();
    }

    public function update($params = null){
        $action = $params['action'];
        $todos  = $params['todos'];

        switch($action){

            case 'mac' : foreach($todos as $key => $value){

                            $this->updateOne($value);
                        }
            break;

            case 'del' :  foreach($todos as $key => $value){

                            $this->delete($value);
                        }
            break;
        }
        return true;
    }

    public function updateOne($id = null){

        $this->tblInstance->update(
                            array(
                                'status' => 4
                            ), array('id'=>$id));
    }

    public function updateAll($id = null , $params = null){
        
        $this->tblInstance->update(
                    array(
                        'title' => $params['title'],
                        'status' => $params['status'],
                        'details' => $params['details']
                    )
                    ,array('id'=>$id));
        return true;    
    }

    public function delete($id = null){

        $this->tblInstance->delete(array('id'=>$id));
    }

    public function search($qry = null){
 
        $rowset = $this->tblInstance->select(function (Select $select) use ($qry) {
                                     $select->where->like('title',"%$qry%");
                                });
        
        return $rowset->toArray();
    }
}