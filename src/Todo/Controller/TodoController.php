<?php
namespace Todo\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Templating\PhpEngine;
use Symfony\Component\Templating\TemplateNameParser;
use Symfony\Component\Templating\Loader\FilesystemLoader;
use Symfony\Component\Templating\Helper\AssetsHelper;
use Symfony\Component\Templating\Helper\SlotsHelper;
use Todo\Model\Todo;

class TodoController
{
	
    /*
      ** Define Template Instance 
    */
    protected $template;

    /*
      ** Define Views Path
    */
	protected $viewspath = __DIR__.'/../views/%name%';

    /*
      ** Define Virtual Host URL
    */
	protected $virtualhost = BASE_URL;

    /*
      ** Define Virtual Host URL
    */
    protected $session;
    /*
    **  Defina Todo Model
    */
    protected $toModel;

    /*
    **
    */
    protected $status = array(1 => 'Pending', 2 => 'In Process', 3 => 'On Hold', 4 => 'Complete');



	public function __construct(){

        $this->session = new Session();
        $this->session->start();

		$loader = new FilesystemLoader($this->viewspath);
    	$this->template = new PhpEngine(new TemplateNameParser(), $loader);
    	$this->template->set(new AssetsHelper());
        $this->template->set(new SlotsHelper());
    	$this->template->set(new AssetsHelper(null, $this->virtualhost));
        $this->toModel = new Todo();

	}

    public function listTodoAction(){
        
        if(!$this->authenticateUser()){

            $this->session->getFlashBag()->add('warning', 'Access denied.');
            return $response = new RedirectResponse(BASE_URL.'/front.php/admin');
        }else{
            echo $this->template->render(
                'list.html.php',
                array(
                    'collection' => $this->toModel ->fetchAll(),
                    'session' => $this->session,
                    'status' => $this->status
                )
            );
        }
    }

    public function addTodoAction(){
        if(!$this->authenticateUser() || $this->session->get('user_role') > 1){

            $this->session->getFlashBag()->add('warning', 'Access denied.');
            return $response = new RedirectResponse(BASE_URL.'/front.php/admin');
        }else{
            echo $this->template->render(
                    'add.html.php',
                    array(
                         'session' => $this->session
                    )
                );
        }
    }
    
    public function postTodoAction(){

        if(!$this->authenticateUser() || $this->session->get('user_role') > 1){

            $this->session->getFlashBag()->add('warning', 'Access denied.');
            return $response = new RedirectResponse(BASE_URL.'/front.php/admin');
        }else{
            $request = Request::createFromGlobals();

            $params = $request->request->all();

            if(count($params) === 3){
                
                $this->toModel->add($params);
                $this->session->getFlashBag()->add('success', 'Todo has been added.');
                return $response = new RedirectResponse(BASE_URL.'/front.php/admin/list_todo');

            }else{
                //Error
                $this->session->getFlashBag()->add('danger', 'All fields are required.');
                return $response = new RedirectResponse(BASE_URL.'/front.php/admin/add_todo');
            }
        }        
    }


    public function updateTodoAction(){

        if(!$this->authenticateUser() || $this->session->get('user_role') > 1){

            $this->session->getFlashBag()->add('warning', 'Access denied.');
            return $response = new RedirectResponse(BASE_URL.'/front.php/admin');
        }else{
             $request = Request::createFromGlobals();

            $params = $request->request->all();
            if(count($params)){
                $this->toModel->updateAll($params['todo_id'],$params);
                $this->session->getFlashBag()->add('success', 'Todo has been updated.');
                return $response = new RedirectResponse(BASE_URL.'/front.php/admin/list_todo');
            }else{
                 $this->session->getFlashBag()->add('danger', 'Something goes wrong..!!');
                 return $response = new RedirectResponse(BASE_URL.'/front.php/admin/list_todo');
            }
        }
    }

    public function updateAllTodoAction(){

        $request = Request::createFromGlobals();

        $params = $request->request->all();

        if(count($params) > 0 ){
            $this->toModel->update($params);
            $this->session->getFlashBag()->add('success', 'Selected Todo"s has been updated.');
            return $response = new RedirectResponse(BASE_URL.'/front.php/admin/list_todo');
        }
        else{
            //Error
            $this->session->getFlashBag()->add('danger', 'Something goes wrong..!!');
            return $response = new RedirectResponse(BASE_URL.'/front.php/admin/list_todo');
        }
    }

    public function editTodoAction($id = null){

        if(!$this->authenticateUser() || $this->session->get('user_role') > 1){

            $this->session->getFlashBag()->add('warning', 'Access denied.');
            return $response = new RedirectResponse(BASE_URL.'/front.php/admin');
        }else{
            if(count($this->toModel->fetchOne($id)) > 0){

                echo $this->template->render(
                        'edit.html.php',
                        array(
                            'todo' => $this->toModel->fetchOne($id),
                            'session' => $this->session,
                        )
                    );
            }else{
                //Error
                $this->session->getFlashBag()->add('danger', 'Todo does not exit.');
                return $response = new RedirectResponse(BASE_URL.'/front.php/admin/list_todo');
            }
        }
    }

    public function deleteTodoAction($id = null){
       
        if(!$this->authenticateUser() || $this->session->get('user_role') > 1){

            $this->session->getFlashBag()->add('warning', 'Access denied.');
            return $response = new RedirectResponse(BASE_URL.'/front.php/admin');
        }else{
             if(count($this->toModel->fetchOne($id)) > 0) {
                $this->toModel->delete($id);
                $this->session->getFlashBag()->add('success', 'Selected Todo"s has been deleted.');
                return $response = new RedirectResponse(BASE_URL.'/front.php/admin/list_todo');
               }else{
                //Error
                    $this->session->getFlashBag()->add('danger', 'Todo does not exit.');
                    return $response = new RedirectResponse(BASE_URL.'/front.php/admin/list_todo');
               }
        }
    }

    public function authenticateUser(){
        
        if($this->session->get('user_role') && count($this->session->all()) > 0){
            return true;
        }
        return false;
    }


    public function searchTodoAction($query = null){
    	
    	$searchResult = $this->toModel->search($query);

    	if(isset($query) && count($searchResult) > 0){
    		echo $this->template->render(
                'list.html.php',
                array(
                    'collection' => $searchResult,
                    'session' => $this->session,
                    'query' => $query,
                )
            );
    	}else if(count($searchResult) == 0){
            $this->session->getFlashBag()->add('warning', 'No record found.');
            return $response = new RedirectResponse(BASE_URL.'/front.php/admin/list_todo');
        }else{
    		
            $this->session->getFlashBag()->add('danger', 'search contain nothing.');
            return $response = new RedirectResponse(BASE_URL.'/front.php/admin/list_todo');
    	}
    	
    }

}
