<?php $view->extend('base.html.php') ?>
<?php $view['slots']->start('body') ?>
<div class="row">
    <div class="col-lg-12">
        <?php
           foreach ($session->getFlashBag()->all() as $type => $messages) {
                foreach ($messages as $message) {
                    echo '<div class="alert alert-'.$type.'"><button class="btn btn-'.$type.' btn-circle" type="button"><i class="fa fa-check"></i></button> '.$message.'</div>';
                }
            }
        ?>
        <h3 class="page-header"><i class="fa fa-th-list"></i> Manage Todo</h3>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
         <form name="listForm" id="listForm" method="post" action="<?php echo $view['assets']->getUrl("front.php/admin/update_todo");?>">
            <div class="panel-heading">
                Todo List
                <?php if($session->get('user_role') == 1):?>
                <div class="pull-right">
                        Action 
                        <select name="action" id="action">
                            <option value="mac">Mark As Completed</option>                        
                            <option value="del">Deleted</option>
                        </select>
                        <button class="btn btn-outline btn-primary btn-xs" type="button" id="actionSubmit">Submit</button>
                </div>
                 <?php endif;?>
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover">
                        <thead>
                            <tr>
                                <?php if($session->get('user_role') == 1):?>
                                <th>#</th>
                                <?php endif;?>
                                <th>Title</th>
                                <th>Status</th>
                                <th>Description</th>
                                <?php if($session->get('user_role') == 1):?>
                                <th>Action</th>
                                <?php endif;?>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach($collection as $_todo):?>
                                <tr>
                                    <?php if($session->get('user_role') == 1):?>
                                    <td>
                                        <input type="checkbox" name="todos[]" value="<?php echo $_todo['id'];?>" />
                                    </td>
                                    <?php endif;?>
                                    <td><?php echo $_todo['title'];?></td>
                                    <td><?php echo $status[$_todo['status']];?></td>
                                    <td><?php echo $_todo['details'];?></td>
                                    <?php if($session->get('user_role') == 1):?>
                                        <td>
                                            <a href="<?php echo BASE_URL;?>/front.php/admin/edit_todo/<?php echo $_todo['id'];?>" class="fa fa-edit ">&nbsp;</a>
                                            <br/>
                                            <a href="<?php echo BASE_URL;?>/front.php/admin/delete_todo/<?php echo $_todo['id'];?>" class="fa fa-trash-o">&nbsp;</a>
                                        </td>   
                                     <?php endif;?>
                                </tr>  
                            <?php endforeach;?>
                        </tbody>
                    </table>
                    
                </div>
                <!-- /.table-responsive -->
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
         </form>
    </div>
</div>
<!-- /.row -->
<?php $view['slots']->stop() ?>