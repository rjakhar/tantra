<?php $view->extend('base.html.php') ?>
<?php $view['slots']->start('body') ?>
<div class="row">
    <div class="col-lg-12">
         <?php
           foreach ($session->getFlashBag()->all() as $type => $messages) {
                foreach ($messages as $message) {
                    echo '<div class="alert alert-'.$type.'"><button class="btn btn-'.$type.' btn-circle" type="button"><i class="fa fa-check"></i></button> '.$message.'</div>';
                }
            }
        ?>
        <h3 class="page-header"><i class="fa  fa-edit"></i> Edit Todo</h3>
    </div>
    <!-- /.col-lg-12 -->
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                Create New Todo
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-lg-12">
                        <form id="todoForm" role="form" method="post" action="<?php echo $view['assets']->getUrl("front.php/admin/update_todo_post");?>">
                            <input type="hidden" name="todo_id" value="<?php echo $todo[0]['id'];?>" />
                            <div class="form-group">
                                <label>Title</label>
                                <input type="text" name="title" class="form-control" value="<?php echo $todo[0]['title'];?>">
                            </div>
                            <div class="form-group">
                                <label>Status</label>
                                <select name="status" class="form-control">
                                    <option value="1" <?php if($todo[0]['status'] == 1):?> selected="selected"<?php endif;?>>Pending</option>
                                    <option value="2" <?php if($todo[0]['status'] == 2):?> selected="selected"<?php endif;?>>In Process</option>
                                    <option value="3" <?php if($todo[0]['status'] == 3):?> selected="selected"<?php endif;?>>On Hold</option>
                                    <option value="4" <?php if($todo[0]['status'] == 4):?> selected="selected"<?php endif;?>>Complete</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Todo Details</label>
                                <textarea name="details" rows="3" class="form-control"> <?php echo $todo[0]['details'];?> </textarea>
                            </div>
                            <button class="btn btn-default" type="submit">Submit Button</button>
                        </form>
                    </div>
                </div>
                <!-- /.row (nested) -->
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>
<?php $view['slots']->stop() ?>
