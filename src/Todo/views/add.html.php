<?php $view->extend('base.html.php') ?>
<?php $view['slots']->start('body') ?>
<div class="row">
    <div class="col-lg-12">
        <?php
           foreach ($session->getFlashBag()->all() as $type => $messages) {
                foreach ($messages as $message) {
                    echo '<div class="alert alert-'.$type.'">'.$message.'</div>';
                }
            }
        ?>
        <h3 class="page-header"><i class="fa fa-plus-circle "></i> Add New Todo Item</h3>
    </div>
    <!-- /.col-lg-12 -->
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                Create New Todo
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-lg-12">
                        <form id="todoForm" role="form" method="post" action="<?php echo $view['assets']->getUrl("front.php/admin/add_todo_post");?>">
                            <div class="form-group">
                                <label>Title</label>
                                <input type="text" name="title" class="form-control" required>
                            </div>
                            <div class="form-group">
                                <label>Status</label>
                                <select name="status" class="form-control" required>
                                    <option value="1">Pending</option>
                                    <option value="2">In Process</option>
                                    <option value="3">On Hold</option>
                                    <option value="4">Complete</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Todo Details</label>
                                <textarea name="details" rows="3" class="form-control" required></textarea>
                            </div>
                            <button class="btn btn-default" type="submit">Submit Button</button>
                        </form>
                    </div>
                </div>
                <!-- /.row (nested) -->
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>
<?php $view['slots']->stop() ?>
