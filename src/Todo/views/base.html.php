<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Admin - Todo List</title>

    <!-- Bootstrap Core CSS -->
    <link href="<?php echo $view['assets']->getUrl('css/bootstrap.min.css');?>" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="<?php echo $view['assets']->getUrl('css/metisMenu.min.css');?>" rel="stylesheet">

     <!-- DataTables CSS -->
    <link href=".<?php echo $view['assets']->getUrl('css/dataTables.bootstrap.css');?>" rel="stylesheet">

    <!-- DataTables Responsive CSS -->
    <link href="<?php echo $view['assets']->getUrl('css/dataTables.responsive.css');?>" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="<?php echo $view['assets']->getUrl('css/sb-admin-2.css');?>" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="<?php echo $view['assets']->getUrl('css/font-awesome.min.css');?>" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="<?php echo BASE_URL;?>/front.php/admin"> <i class="fa fa-tumblr-square"></i> Tantra Admin</a>
            </div>
            <!-- /.navbar-header -->

            <ul class="nav navbar-top-links navbar-right">
                <!-- /.dropdown -->
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user fa-fw"></i>  <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        
                        <li><a href="<?php echo BASE_URL;?>/front.php/admin/logout"><i class="fa fa-power-off"></i>  Logout</a>
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->
            </ul>
            <!-- /.navbar-top-links -->

            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        <li class="sidebar-search">
                            <div class="input-group custom-search-form">
                                <input type="text" class="form-control" name="searchQry" value="<?php echo $query;?>" id="searchQry" placeholder="Search Todo By Title...">
                                <span class="input-group-btn">
                                <button class="btn btn-default" type="button" id="searchTodo">
                                    <i class="fa fa-search"></i>
                                </button>
                            </span>
                            </div>
                            <!-- /input-group -->
                        </li>
                        <li>
                            <a href="javascript:void(0);">
                                <i class="fa fa-tasks fa-fw"></i> ToDo List
                                <span class="fa arrow"></span>
                            </a>
                            <ul class="nav nav-second-level collapse" aria-expanded="false" style="height: 0px;">
                                <li>
                                    <a href="<?php echo BASE_URL;?>/front.php/admin/list_todo">Manage Todo</a>
                                </li>
                                <?php if($session->get('user_role') == 1):?>
                                <li>
                                    <a href="<?php echo BASE_URL;?>/front.php/admin/add_todo">Add Todo</a>
                                </li>
                                <?php endif;?>
                            </ul>
                        </li>
                       
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>

        <div id="page-wrapper">
           
             <?php $view['slots']->output('body') ?>
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="<?php echo $view['assets']->getUrl('js/jquery.min.js');?>"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo $view['assets']->getUrl('js/bootstrap.min.js');?>"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="<?php echo $view['assets']->getUrl('js/metisMenu.min.js');?>"></script>

     <!-- DataTables JavaScript -->
    <script src="<?php echo $view['assets']->getUrl('js/jquery.dataTables.min.js');?>"></script>
    <script src="<?php echo $view['assets']->getUrl('js/dataTables.bootstrap.min.js');?>"></script>

    <!-- Custom Theme JavaScript -->
    <script src="<?php echo $view['assets']->getUrl('js/sb-admin-2.js');?>"></script>
    <script>
            $('#actionSubmit').click(function(){
                var action = $(this).val();
                var atLeastOneIsChecked = $('input[name="todos[]"]:checked').length > 0;
                

                if(!atLeastOneIsChecked){
                    alert('Please select at least one record.');
                    return ;
                }else{
                    $( "#listForm" ).submit();    
                }
            });
        
            $('#searchTodo').click(function(){
                var qry = $('#searchQry').val();
                if(qry.length > 2){
                    var urlToRedirect = "<?php echo BASE_URL;?>/front.php/admin/search_todo/"+qry;
                    window.location.href = urlToRedirect;
                }else{
                    alert('Please enter at least 3 letters.');
                    return false;
                }
            });
    </script>
</body>

</html>
