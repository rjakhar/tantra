<?php
use Symfony\Component\Routing;

$routes = new Routing\RouteCollection();



$routes->add('admin_login', new Routing\Route('/admin', array(
    '_controller' => 'Admin\\Controller\\LoginController::indexAction',
)));

$routes->add('admin_login_action', new Routing\Route('/admin/login', array(
    '_controller' => 'Admin\\Controller\\LoginController::loginAction',
)));

$routes->add('admin_logout', new Routing\Route('/admin/logout', array(
    '_controller' => 'Admin\\Controller\\LoginController::logoutAction',
)));

$routes->add('admin_add_todo', new Routing\Route('/admin/add_todo', array(
    '_controller' => 'Todo\\Controller\\TodoController::addTodoAction',
)));

$routes->add('admin_add_todo_action', new Routing\Route('/admin/add_todo_post', array(
    '_controller' => 'Todo\\Controller\\TodoController::postTodoAction',
)));

$routes->add('admin_edit_todo', new Routing\Route('/admin/edit_todo/{id}', array(
    '_controller' => 'Todo\\Controller\\TodoController::editTodoAction',
    'id' => null,
)));

$routes->add('admin_update_todo_action', new Routing\Route('/admin/update_todo_post', array(
    '_controller' => 'Todo\\Controller\\TodoController::updateTodoAction',
)));

$routes->add('admin_delete_todo', new Routing\Route('/admin/delete_todo/{id}', array(
    '_controller' => 'Todo\\Controller\\TodoController::deleteTodoAction',
    'id' => null,
)));

$routes->add('admin_list_todo', new Routing\Route('/admin/list_todo', array(
    '_controller' => 'Todo\\Controller\\TodoController::listTodoAction',
)));

$routes->add('admin_updateall_todo_action', new Routing\Route('/admin/update_todo', array(
    '_controller' => 'Todo\\Controller\\TodoController::updateAllTodoAction',
)));

$routes->add('admin_search_todo', new Routing\Route('/admin/search_todo/{query}', array(
    '_controller' => 'Todo\\Controller\\TodoController::searchTodoAction',
    'query' => null,
)));

return $routes;