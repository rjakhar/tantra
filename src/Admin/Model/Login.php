<?php
namespace Admin\Model;

use Zend\Db\Adapter\Adapter;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Db\ResultSet\HydratingResultSet;

class Login
{

	/*
	  ** 
	*/
	protected $userTable = 'users';

	/*
      ** Define Adapter Instance 
    */
    protected $adapter;

    /*
   	  **
	*/
	protected $tblInstance;


    public function __construct()
    {	
    

    	$this->adapter = new Adapter(
    					array
    						(
    						'driver'   => DRIVER,
    						'database' => DB_NAME,
    						'username' => DB_USER,
    						'password' => DB_PASSWORD
						)
    				);

		$this->tblInstance = new TableGateway(
								$this->userTable, 
								$this->adapter, 
								null,
								new HydratingResultSet()
							);
    }


    public function validateUser($params = null, $sessionInstance){
        
        $email = $params['email'];
        $password = md5($params['password']);

        $result = $this->getUserByEmail($email);
        
        if(count($result[0])){
            if($password === $result[0]['password']){

                if($result[0]['is_admin'] == 1){
                    $sessionInstance->set('user_role',1);
                }else{
                    $sessionInstance->set('user_role',2);
                }
            }else{
                //Error Password not match
                $sessionInstance->remove('user_role');

                $sessionInstance->getFlashBag()->add('danger', 'Password do not match.');
            }
        }else{
            //Error Email not found
            $sessionInstance->remove('user_role');
            $sessionInstance->getFlashBag()->add('danger', 'Email not found.');
        }
        return $this;
    }


    protected function getUserByEmail($email = null){
        
        $rowset = $this->tblInstance->select(array( 'email' => $email ));
        return $rowset->toArray();
    }

    
}