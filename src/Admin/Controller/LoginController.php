<?php
namespace Admin\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Templating\PhpEngine;
use Symfony\Component\Templating\TemplateNameParser;
use Symfony\Component\Templating\Loader\FilesystemLoader;
use Symfony\Component\Templating\Helper\AssetsHelper;
use Symfony\Component\Templating\Helper\SlotsHelper;
use Admin\Model\Login;

class LoginController
{
	
    /*
      ** Define Template Instance 
    */
    protected $template;

    /*
      ** Define Views Path
    */
	protected $viewspath = __DIR__.'/../views/%name%';

    /*
      ** Define Virtual Host URL
    */
	protected $virtualhost = BASE_URL;

    /*
      ** Define Virtual Host URL
    */
    protected $session;
    /*
    **  Define User Model
    */
    protected $loginModel;




	public function __construct(){

        $this->session = new Session();
        $this->session->start();

		$loader = new FilesystemLoader($this->viewspath);
    	$this->template = new PhpEngine(new TemplateNameParser(), $loader);
    	$this->template->set(new AssetsHelper());
        $this->template->set(new SlotsHelper());
    	$this->template->set(new AssetsHelper(null, $this->virtualhost));
        $this->loginModel = new Login();
	}

    public function indexAction()
    {//echo $this->session->get('user_role');exit;
        if($this->session->get('user_role')){
            return $response = new RedirectResponse(BASE_URL.'/front.php/admin/list_todo');
        }else{
                echo $this->template->render(
                'login.html.php',
                    array(
                        'session' => $this->session
                    )
            );
        }        
    }

    public function loginAction()
    {
        $request = Request::createFromGlobals();
        $params = $request->request->all();

        if(!empty($params['email']) && !empty($params['password'])){
            
            $user = $this->loginModel->validateUser($params,$this->session); 

            if($this->session->get('user_role')){
                 return $response = new RedirectResponse(BASE_URL.'/front.php/admin/list_todo');
            }else{
                return $response = new RedirectResponse(BASE_URL.'/front.php/admin');
            }
        }else{
            //If any field is blank
            $this->session->getFlashBag()->add('danger', 'Both fields are required.');
            return $response = new RedirectResponse(BASE_URL.'/front.php/admin');
        }

    }

    public function logoutAction(){
        $this->session->remove('user_role');
        $this->session->clear();
        return $response = new RedirectResponse(BASE_URL.'/front.php/admin');
    }
}