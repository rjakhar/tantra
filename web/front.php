<?php
/*
** Define Constant
*/
define(BASE_URL,'http://127.0.0.1:8181');
define(DRIVER,'pdo_mysql');
define(DB_NAME,'redbox');
define(DB_USER,'root');
define(DB_PASSWORD,'root');

/*@ End @*/
error_reporting(E_ALL);

require_once __DIR__.'/../vendor/autoload.php';

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing;
use Symfony\Component\HttpKernel;


$request = Request::createFromGlobals();
$routes = include __DIR__.'/../src/route.php';

$context = new Routing\RequestContext();
$matcher = new Routing\Matcher\UrlMatcher($routes, $context);
$resolver = new HttpKernel\Controller\ControllerResolver();
	
$framework = new Tantra\Tantra($matcher, $resolver);

$response = $framework->handle($request);

$response->send();
